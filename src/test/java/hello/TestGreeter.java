package hello;

// on line change

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


// Starting class (3-4)


public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Alex name test")
   public void testAlex(){
      g.setName("Alex");
      assertEquals(g.getName(),"Alex");
      assertEquals(g.sayHello(),"Hello Alex!");
   }

   @Test
   @DisplayName("Test for empty name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Ted")
   public void testGreeterTed() 

   {
      g.setName("Ted");
      assertEquals(g.getName(),"Ted");
      assertEquals(g.sayHello(),"Hello Ted!");
   }

   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

   @Test
   @DisplayName("Test for Name='Colton'")
   public void testGreeterColton() 
   {

      g.setName("Colton");
      assertEquals(g.getName(),"Colton");
      assertEquals(g.sayHello(),"Hello Colton!");
   }

   @Test
   @DisplayName("Test for not Colton ")
   public void testGreeterNotSame() 
   {

      boolean flag;
      String name = "Colton";
      if(name != "Colton")
        flag = true;
      else
        flag = false;
      assertFalse(flag, "NotSame as Colton!");
   }

   @Test
   @DisplayName("Test for assignment fourteen")
   public void testGreeterNewTest()
   {
       g.setName("fourteen");
       assertEquals(g.getName(), "fourteen");
       assertEquals(g.sayHello(), "Hello fourteen!");
   }

   @Test
   @DisplayName("Test for Name='Gabriel'")
   public void testGreeterMyName() 
   {

      g.setName("Gabriel");
      assertEquals(g.getName(),"Gabriel");
      assertEquals(g.sayHello(),"Hello Gabriel!");
   }

   @Test
   @DisplayName("Unique Test for Name != 'Gabriel'")
   public void testGreeterNotMyNAme() 
   {
       // ideally this is different enough from the other's test cases, there is only so much that can be done with assertFalse
       // and the current set up

      g.setName("Ted");
      assertFalse(g.getName() == "Gabriel", "The name is actually 'Gabriel'");

   }

   @Test
   @DisplayName("Test for Name = 'Eastland'")
   public void testGreeterEastland() 

   {
      g.setName("Eastland");
      assertEquals(g.getName(),"Eastland");
      assertEquals(g.sayHello(),"Hello Eastland!");
   }

   @Test
   @DisplayName("Test for Eastlands Assert False")
   public void testGreeterEastlandAssertFalseDontCopyMe() 
   {
      g.setName("Eastland");
      assertEquals(g.getName(),"Eastland");
      assertFalse(g.getName() == "Poop");

   }





   @Test
   @DisplayName("Test for Name='Ryan'")
   public void testGreeterRyan() 
   {

      g.setName("Ryan");
      assertEquals(g.getName(),"Ryan");
      assertEquals(g.sayHello(),"Hello Ryan!");
   }

   @Test
   @DisplayName("Test for Scott's name vs Ryan's name")
   public void testScottOrRyan()
   {
      g.setName("Scott");
      assertFalse(g.getName() == "Ryan");
      assertEquals(g.getName(), "Scott");

      g.setName("Ryan");
      assertFalse(g.getName() == "Scott");
      assertEquals(g.getName(), "Ryan");
   }

}
